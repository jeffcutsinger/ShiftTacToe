use shift_tac_toe::{NodePointer, Position, WinLoseDrawNode};
use shift_tac_toe::slap::LockedSlap;

use crossterm::execute;
use crossterm::cursor;
use crossterm::event::{Event, KeyCode, KeyEventKind, read};
use crossterm::terminal::{self, ClearType, EnterAlternateScreen, LeaveAlternateScreen};

use anyhow::{Result};

use std::cell::Cell;
use std::io::{self, Write, StdoutLock};


pub struct TreeExplorer<'tree, 'locked> {
    tree: &'locked LockedSlap<'tree, WinLoseDrawNode<NodePointer>>,
    ancestors: Vec<NodePointer>,
    ancestor_positions: Vec<Position>,
    focus_pointer: NodePointer,
    focus_position: Position,
    children: Vec<NodePointer>,
    _child_positions: Vec<NodePointer>,
    output: Cell<Option<StdoutLock<'static>>>,
}

impl<'locked, 'tree> TreeExplorer<'locked, 'tree> {
    pub fn new(tree: &'locked LockedSlap<'tree, WinLoseDrawNode<NodePointer>>, focus_pointer: NodePointer) -> Result<Self> {
        let mut output = io::stdout().lock();
        let enter_result = execute!(output, EnterAlternateScreen);
        let raw_mode_result = terminal::enable_raw_mode();
        let mut result = Self {
            tree,
            focus_pointer: focus_pointer.clone(),
            focus_position: Default::default(),
            ancestors: vec![],
            ancestor_positions: vec![],
            children: vec![],
            _child_positions: vec![],
            output: Cell::new(Some(output)),
        };
        raw_mode_result?;
        enter_result?;
        result.update(focus_pointer)?;
        Ok(result)
    }

    fn update(&mut self, focus_pointer: NodePointer) -> Result<()> {
        self.focus_pointer = focus_pointer.clone();
        let focus = (&focus_pointer).as_ref();

        self.ancestors.clear();
        self.ancestor_positions.clear();
        focus.get_ancestors(&mut self.ancestors);
        focus_pointer.get_positions(&mut self.ancestor_positions, self.ancestors.as_slice());

        if let Some(position) = self.ancestor_positions.last() {
            self.focus_position = position.clone();
        }

        self.children.clear();
        for node_pointer in self.tree {
            let node = unsafe { node_pointer.as_ref() };
            if node.parent().as_ref() == Some(&focus_pointer) {
                self.children.push(node_pointer.into());
            }
        }

        Ok(())
    }

    fn clear(&self) {
        let mut output = self.output.take().unwrap();
        execute!(output, terminal::Clear(ClearType::All), cursor::MoveTo(0, 0)).unwrap();
        self.output.set(Some(output));
    }

    fn write(&self, text: impl AsRef<str>) {
        let mut output = self.output.take().unwrap();
        Self::_write_with_newlines(&mut output, text);
        self.output.set(Some(output));
    }

    fn writeln(&self, text: impl AsRef<str>) {
        let mut output = self.output.take().unwrap();
        Self::_write_with_newlines(&mut output, text);
        execute!(output, cursor::MoveToNextLine(1)).unwrap();
        self.output.set(Some(output));
    }

    fn _write_with_newlines(output: &mut StdoutLock<'_>, text: impl AsRef<str>) {
        let line_count = text.as_ref().lines().count();
        for (index, line) in text.as_ref().lines().enumerate() {
            write!(output, "{}", line.trim_end_matches('\n')).unwrap();
            if index + 1 != line_count {
                execute!(output, cursor::MoveToNextLine(1)).unwrap();
            }
        }
    }

    pub fn explore(&mut self) -> Result<()> {
        let mut end = false;
        loop {
            self.print();

            loop {
                match read().unwrap() {
                    Event::Key(e) if e.kind == KeyEventKind::Press => {
                        match e.code {
                            KeyCode::Char('q') => {
                                end = true;
                                break;
                            }
                            KeyCode::Char(c) if c != '0' => {
                                if let Some(digit) = c.to_digit(10) {
                                    let digit = digit as usize;
                                    if let Some(child) = self.children.get(digit - 1) {
                                        self.update(child.clone())?;
                                        break;
                                    }
                                }
                            },
                            KeyCode::Left => {
                                if let Some(parent_pointer) = self.focus_pointer.as_ref().parent() {
                                    self.update(parent_pointer)?;
                                    break;
                                }
                            },
                            _ => {},
                        }
                    },
                    _ => {},
                }
            }

            if end {
                break;
            }
        }
        Ok(())
    }

    fn focus_parent(&self) -> Option<NodePointer> {
        self.focus_pointer.as_ref().parent()
    }

    fn focus_as_stats_string(&self) -> String {
        self.focus_pointer.as_ref().as_stats_string()
    }

    pub fn print(&mut self) {
        self.clear();

        if self.focus_parent().is_some() {
            for line_index in 0..Position::line_count_2d_string() {
                for ancestor_index in 0..self.ancestors.len() {
                    let ancestor_position = &self.ancestor_positions[ancestor_index];
                    let ancestor_position_2d_string = ancestor_position.as_2d_string();
                    let ancestor_position_2d_strings: Vec<&str> = ancestor_position_2d_string.split('\n').collect();
                    let maybe_arrow =
                        if line_index == Position::middle_line_2d_string() {
                            "->"
                        } else {
                            "  "
                        };
                    self.write(ancestor_position_2d_strings[line_index]);

                    let at_end = ancestor_index == self.ancestors.len() - 1;
                    if !at_end {
                        self.write(maybe_arrow);
                    } else {
                        self.writeln("");
                    }
                }
            }
        }

        // self.writeln("");
        // self.writeln("****************");
        // self.writeln("* CURRENT NODE *");
        // self.writeln("****************");
        // self.writeln("");

        self.writeln(format!("{:?}", self.focus_pointer));
        self.writeln(self.focus_position.as_2d_string());
        self.writeln(self.focus_as_stats_string());

        self.writeln("|");
        for (child_index, child_pointer) in self.children.iter().enumerate() {
            self.writeln("|");
            let child = child_pointer.as_ref();
            let child_position = self.focus_position.make_play(child.play().unwrap()).unwrap();
            let child_position_2d_string = child_position.as_2d_string();
            let child_position_2d_strings: Vec<&str> = child_position_2d_string.split('\n').map(|s| s.trim_end_matches('\n')).collect();
            for line_index in 0..Position::line_count_2d_string() {
                let maybe_number = if line_index == 0 {
                    format!("{}:", child_index + 1)
                } else {
                    "  ".to_string()
                };
                self.write(format!("| {maybe_number} {}", child_position_2d_strings[line_index]));
                if line_index == 0 {
                    self.write(format!(" {}", child.as_stats_string()));
                }
                self.writeln("");
            }
        }

    }
}

impl Drop for TreeExplorer<'_, '_> {
    fn drop(&mut self) {
        let raw_mode = terminal::disable_raw_mode();
        if let Some(mut output) = self.output.take() {
            execute!(output, LeaveAlternateScreen).unwrap();
        }
        raw_mode.unwrap();
    }
}
