mod tree_explorer;

use tree_explorer::TreeExplorer;

use shift_tac_toe::{ThreadSubscriberFactory, WinLoseDrawTree, WinLoseDrawTreeParameters};

use crossterm::terminal;

use clap::Parser;

use time::{UtcOffset, OffsetDateTime};
use tracing_appender::non_blocking::WorkerGuard;

use tracing_subscriber::{EnvFilter, fmt, prelude::*};

use tracing_unwrap::ResultExt;

use tracing::{debug, trace};

use anyhow::Result;

use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::io::ErrorKind;
use std::path::{Path, PathBuf};
use std::panic;
use std::process;
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;


#[derive(Debug, Parser)]
struct Arguments {
    #[arg(long)]
    approximate_number_threads: Option<usize>,

    #[arg(long)]
    starting_max_depth: Option<usize>,

    #[arg(long)]
    log_directory: Option<PathBuf>
}

#[derive(Clone)]
pub struct LogDirectory {
    path: PathBuf
}

impl LogDirectory {
    pub fn new(root_logging_directory: PathBuf, utc_offset: UtcOffset) -> Self {
        let now = OffsetDateTime::now_utc().to_offset(utc_offset).format(&time::format_description::well_known::Rfc3339).expect("Formatting time failed");
        let subdirectory = format!("run-{now}");
        let logs_directory = root_logging_directory.join(&subdirectory);
        fs::create_dir_all(&logs_directory).expect(&format!("Could not create logs directory {}", logs_directory.display()));
        let latest_symlink_path = root_logging_directory.join("latest");
        if let Err(error) = fs::remove_file(&latest_symlink_path) {
            if error.kind() != ErrorKind::NotFound {
                panic!("Could not remove latest symlink: {error}");
            }
        }
        std::os::unix::fs::symlink(&subdirectory, &latest_symlink_path).expect("Could not create latest symlink");
        Self { path: logs_directory }
    }

    pub fn path(&self) -> &Path {
        self.path.as_path()
    }
}

fn create_file_subscriber(log_directory: &LogDirectory, filename: impl AsRef<str>) -> Result<(Arc<dyn tracing::Subscriber + Send + Sync>, WorkerGuard), std::io::Error> {
    let log_path = log_directory.path().join(filename.as_ref());
    let (file_appender, worker_guard) = tracing_appender::non_blocking(fs::File::create(log_path)?);
    Ok((Arc::new(
        tracing_subscriber::registry()
            .with(tracing_subscriber::fmt::layer()
                .with_ansi(false)
                .with_writer(file_appender))
            .with(EnvFilter::from_env("STT_LOG"))
            ), worker_guard))
}

pub struct FilePerThreadSubscriberFactory {
    log_directory: Arc<LogDirectory>,
    worker_guards_mutex: Mutex<Vec<WorkerGuard>>
}

impl FilePerThreadSubscriberFactory {
    pub fn new(log_directory: Arc<LogDirectory>) -> Result<Self> {
        Ok(Self { log_directory, worker_guards_mutex: Default::default() })
    }
}

impl ThreadSubscriberFactory for FilePerThreadSubscriberFactory {
    fn create(&self, thread_type: &str, thread_number: usize) -> Result<Arc<dyn tracing::Subscriber + Send + Sync>, Box<dyn Error>> {
        let (subscriber, worker_guard) = create_file_subscriber(self.log_directory.as_ref(), format!("{thread_type}-{thread_number}.log"))?;
        {
            let mut worker_guards = self.worker_guards_mutex.lock().expect("Worker guards mutex was poisoned.");
            worker_guards.push(worker_guard);
        }
        Ok(subscriber)
    }
}

#[derive(Clone, Copy, Eq, PartialEq)]
enum RunResult {
    IncrementDepth,
    Stop,
}

fn main() {
    let default_hook = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        let _error = terminal::disable_raw_mode();
        default_hook(panic_info);
        process::abort();
    }));

    if let Err(error) = tracing::subscriber::set_global_default(
        tracing_subscriber::registry()
            .with(fmt::layer())
            .with(EnvFilter::from_env("STT_LOG"))) {
        eprintln!("Could not set fallback logging configuration: {error}")
    };

    let arguments = Arguments::parse();
    let root_log_directory = arguments.log_directory.unwrap_or("stt_logs".into());
    let approximate_number_threads = arguments.approximate_number_threads.unwrap_or_else(|| usize::from(thread::available_parallelism().unwrap_or(1.try_into().expect("1 is 0?"))));
    let mut max_depth = arguments.starting_max_depth.unwrap_or(2);
    // OffsetDateTime::now_local() failed on subsequent calls of run(), so I get the offset here and use now_utc().to_offset(...)
    let utc_offset = UtcOffset::current_local_offset().expect("Getting local offset failed");
    while run(&root_log_directory, approximate_number_threads, max_depth, utc_offset) == RunResult::IncrementDepth {
        max_depth += 1;
    }
}

fn run(root_log_directory: &Path, approximate_number_threads: usize, max_depth: usize, utc_offset: UtcOffset) -> RunResult {
    let log_directory = Arc::new(LogDirectory::new(root_log_directory.to_owned(), utc_offset));
    debug!("Log directory: {}", log_directory.path().display());

    let (subscriber, _worker_guard) = create_file_subscriber(log_directory.as_ref(), "_main.log").unwrap_or_log();

    return tracing::subscriber::with_default(subscriber, move || {
        trace!("Building WLD tree.");
        let parameters = WinLoseDrawTreeParameters::new(
            approximate_number_threads,
            0,
            max_depth,
            FilePerThreadSubscriberFactory::new(log_directory.clone()).unwrap());
        let wld_tree = WinLoseDrawTree::build(&parameters);
        thread::sleep(Duration::from_millis(100)); // Give the diagnostics threads time to finish their job.
        let node_count = wld_tree.node_count();
        assert!(node_count > 0);
        let Some(root_node_pointer) = wld_tree.root_node() else {
            panic!("No root node");
        };
        let root_node = root_node_pointer.as_ref();
        let locked_tree = wld_tree.lock_nodes();
        eprintln!("");
        eprintln!("Node count: {}", node_count);
        if !root_node.is_tallied() {
            trace!("Not all nodes got tallied.");
            trace!("root: {:?}, {}", root_node_pointer, root_node.as_stats_string());
            let mut untallied_nodes_and_children = HashMap::new();
            for node_pointer in &locked_tree {
                let node = unsafe { node_pointer.as_ref() };
                let Some(parent_pointer) = node.parent() else {
                    continue;
                };
                if parent_pointer.parent().is_some() && !parent_pointer.as_ref().is_tallied() {
                    if !untallied_nodes_and_children.contains_key(&parent_pointer) {
                        untallied_nodes_and_children.insert(parent_pointer.clone(), vec![]);
                    }
                    let siblings = untallied_nodes_and_children.get_mut(&parent_pointer).unwrap();
                    siblings.push(node_pointer);
                }
            }
            for (parent_pointer, child_pointers) in untallied_nodes_and_children.iter() {
                trace!("|  {:?}, parent: {:?}, {} not tallied", parent_pointer, parent_pointer.as_ref().parent().unwrap(), parent_pointer.as_ref().as_stats_string());
                for child_pointer in child_pointers {
                    trace!("|  |  {:?} {}", child_pointer, unsafe { child_pointer.as_ref() }.as_stats_string());
                }
            }

            let r;
            {
                r = TreeExplorer::new(&locked_tree, root_node_pointer).and_then(|mut t| t.explore());
            }
            r.unwrap();

            return RunResult::Stop;
        }
        if !root_node.conclusion().is_some() {
            eprintln!("Conclusion not found. Incrementing depth.");
            return RunResult::IncrementDepth;
        }
        return RunResult::Stop;
    });
}
