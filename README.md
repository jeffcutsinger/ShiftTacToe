# Shift-Tac-Toe

Shift-Tac-Toe is a game similar to Connect Four but with an additional mechanic: sliding rows to the left and right. This repository contains a basic implementation of the rules and a win-lose-draw tree in an attempt to find properties of the game with optimal play.

It is currently in pre-alpha; the cli does nothing but build the tree and dump information about its size & root node. Eventually it should be fully playable.

# Running

This implementation is in Rust; [install the rust compiler and tools](https://www.rust-lang.org/tools/install) however you wish. Then run `cargo run cli` from the root of this repository to compile and run the program. Setting the `STT_LOG` environment variable will control the verbosity of output. `STT_LOG=info` will display node counts as the tree is constructed. `STT_LOG=trace` will output detailed information about the tree as it is being constructed. Supplying a regular expression after a `/` will filter the log output. For example `STT_LOG=trace/(NODE|CONSTRUCTION)` will display any log output so long as it starts with "NODE" or "CONSTRUCTION". More details are available from the [`env_logger` crate documentation](https://docs.rs/env_logger).

# License

Copyright 2023 Jeff Cutsinger.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.