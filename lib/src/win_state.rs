use crate::player::Player;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum WinState {
    PlayerOne,
    PlayerTwo,
    PlayersOneAndTwo,
}

impl From<Player> for WinState {
    fn from(player: Player) -> Self {
        match player {
            Player::One => Self::PlayerOne,
            Player::Two => Self::PlayerTwo,
        }
    }
}

pub(crate) trait WinStateExt {
    fn with_player_winning(self, player: Player) -> WinState;
}
impl WinStateExt for Option<WinState> {
    fn with_player_winning(self, player: Player) -> WinState {
        match (self, player) {
            (None, p) => p.into(),
            (Some(WinState::PlayerOne), Player::Two) => WinState::PlayersOneAndTwo,
            (Some(WinState::PlayerTwo), Player::One) => WinState::PlayersOneAndTwo,
            (Some(s), _) => s
        }
    }
}
