use crate::{player::Player, play::Play, win_state::WinState, constants::{ROW_COUNT_USIZE, COLUMN_COUNT_USIZE, ROW_COUNT, COLUMN_COUNT}, row_position::RowPosition, grid::Grid};

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub struct Position {
    grid: Grid,
    row_positions: [RowPosition; ROW_COUNT_USIZE],
    player: Player,
    win_state: Option<WinState>,
}

impl Default for Position {
    fn default() -> Self {
        Self { player: Player::One, grid: Default::default(), row_positions: Default::default(), win_state: Default::default() }
    }
}

impl Position {
    pub fn make_play(&self, play: Play) -> Result<Position, ()> {
        let mut new_row_positions = self.row_positions.clone();
        let mut new_grid = self.grid.clone();
        match play {
            Play::ShiftLeft { row } => {
                assert!(row < ROW_COUNT);

                new_row_positions[row as usize] = match self.row_positions[row as usize].shift_left() {
                    Some(p) => p,
                    None => return Err(()),
                };

                new_grid.shift_left(row);
            },
            Play::ShiftRight { row } => {
                assert!(row < ROW_COUNT);

                new_row_positions[row as usize] = match self.row_positions[row as usize].shift_right() {
                    Some(p) => p,
                    None => return Err(()),
                };

                new_grid.shift_right(row);
            },
            Play::DropPiece { column } => {
                assert!(column < COLUMN_COUNT);

                if !new_grid.drop_piece(column, self.player) {
                    return Err(());
                }
            },
        }
        let new_player = self.player.next();
        Ok(
            Self {
                grid: new_grid,
                row_positions: new_row_positions,
                win_state: new_grid.win_state(),
                player: new_player,
            }
        )
    }

    pub fn player(&self) -> Player {
        self.player
    }

    pub fn win_state(&self) -> Option<WinState> {
        self.win_state
    }

    pub fn as_one_line_string(&self) -> String {
        let mut result = String::new();

        for row in 0..ROW_COUNT {
            result += match self.row_positions[row as usize]{
                RowPosition::Left => "L",
                RowPosition::Center => "C",
                RowPosition::Right => "R",
            };

            for column in 0..COLUMN_COUNT {
                result += match self.grid.get(row, column) {
                    None => "0",
                    Some(Player::One) => "1",
                    Some(Player::Two) => "2",
                }
            }
        }
        result += match self.player {
            Player::One => "1",
            Player::Two => "2",
        };
        result += match self.win_state {
            None => "N",
            Some(WinState::PlayerOne) => "1",
            Some(WinState::PlayerTwo) => "2",
            Some(WinState::PlayersOneAndTwo) => "B",
        };

        result
    }

    pub fn line_count_2d_string() -> usize {
        ROW_COUNT_USIZE + 2
    }

    pub fn middle_line_2d_string() -> usize {
        match ROW_COUNT_USIZE {
            3 => 3,
            _other => Self::line_count_2d_string() / 2,
        }
    }

    pub fn as_2d_string(&self) -> String {
        let mut result = String::new();

        for _ in 0..(COLUMN_COUNT_USIZE+4) {
            result += "-";
        }
        for row in 0..ROW_COUNT {
            result += "\n";

            if self.row_positions[row as usize] == RowPosition::Left {
                result += "|"
            } else {
                result += " "
            }
            result += "|";

            for column in 0..COLUMN_COUNT {
                result += match self.grid.get(row, column) {
                    None => "0",
                    Some(Player::One) => "1",
                    Some(Player::Two) => "2",
                }
            }

            result += "|";
            if self.row_positions[row as usize] == RowPosition::Right {
                result += "|"
            } else {
                result += " "
            }
        }
        result += "\n";
        for _ in 0..(COLUMN_COUNT_USIZE+4) {
            result += "-";
        }
        result += "\n";
        result += match self.win_state {
            None => match self.player {
                Player::One => "1's turn",
                Player::Two => "2's turn",
            },
            Some(WinState::PlayerOne) => "1 wins",
            Some(WinState::PlayerTwo) => "2 wins",
            Some(WinState::PlayersOneAndTwo) => "both win",
        };
        result
    }
}

#[cfg(all(test, feature="three_by_three"))]
mod tests {
    use super::*;

    #[test]
    pub fn fill_left_column() {
        let position = Position::default();
        let play = Play::DropPiece { column: 0 };
        let Ok(position) = position.make_play(play) else {
            panic!("{play:?} on {position:?} was regarded as invalid.");
        };

        assert_eq!(position,
          Position {
            grid: Grid {
                array:
                [
                    [None, None, None],
                    [None, None, None],
                    [Some(Player::One), None, None],
                ],
            },
            row_positions: [
                RowPosition::Center,
                RowPosition::Center,
                RowPosition::Center,
            ],
            win_state: None,
            player: Player::Two,
        });

        let Ok(position) = position.make_play(play) else {
            panic!("{play:?} on {position:?} was regarded as invalid.");
        };

        assert_eq!(position,
          Position {
            grid: Grid {
                array:
                [
                    [None, None, None],
                    [Some(Player::Two), None, None],
                    [Some(Player::One), None, None],
                ],
            },
            row_positions: [
                RowPosition::Center,
                RowPosition::Center,
                RowPosition::Center,
            ],
            win_state: None,
            player: Player::One
        });

        let Ok(position) = position.make_play(play) else {
            panic!("{play:?} on {position:?} was regarded as invalid.");
        };

        assert_eq!(position,
          Position {
            grid: Grid {
                array:
                [
                    [Some(Player::One), None, None],
                    [Some(Player::Two), None, None],
                    [Some(Player::One), None, None],
                ],
            },
            row_positions: [
                RowPosition::Center,
                RowPosition::Center,
                RowPosition::Center,
            ],
            win_state: None,
            player: Player::Two
        });
    }

    #[test]
    fn bottom_row_left_shift_drops_top_two_pieces_on_left_column() {
        let position = Position {
            grid: Grid {
                array:
                [
                    [Some(Player::One), None, None],
                    [Some(Player::Two), None, None],
                    [Some(Player::One), None, None],
                ],
            },
            row_positions: [
                RowPosition::Center,
                RowPosition::Center,
                RowPosition::Center,
            ],
            win_state: None,
            player: Player::One,
        };
        let play = Play::ShiftLeft { row: 2 };

        let Ok(position) = position.make_play(play) else {
            panic!("{play:?} on {position:?} was regarded as invalid.");
        };

        assert_eq!(
            position,
            Position {
                grid: Grid {
                    array:
                    [
                        [None, None, None],
                        [Some(Player::One), None, None],
                        [Some(Player::Two), None, None],
                    ],
                },
                row_positions: [
                    RowPosition::Center,
                    RowPosition::Center,
                    RowPosition::Left,
                ],
                win_state: None,
                player: Player::Two,
            }
        )
    }

}
