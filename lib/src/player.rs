#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum Player {
    One, Two
}

impl Player {
    pub fn next(self) -> Self {
        match self {
            Self::One => Self::Two,
            Self::Two => Self::One,
        }
    }
}
