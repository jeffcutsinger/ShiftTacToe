
mod compact;
mod conclusion;
pub mod constants;
mod grid;
mod play;
mod player;
mod position;
mod row_position;
mod thread_logging;
mod win_state;
mod wld_tree;
pub mod slap;

pub use thread_logging::ThreadSubscriberFactory;
pub use wld_tree::{NodePointer, WinLoseDrawTree, WinLoseDrawNode};
pub use wld_tree::ConstructionParameters as WinLoseDrawTreeParameters;
pub use position::Position;

#[cfg(test)]
mod tests {
    use tracing_appender::non_blocking::WorkerGuard;

    use once_cell::sync::Lazy;

    use std::sync::Mutex;

    static WORKER_GUARD: Lazy<Mutex<Option<WorkerGuard>>> = Lazy::new(|| Default::default());
    #[cfg(test)]
    #[ctor::ctor]
    fn setup_logging() {
        let (non_blocking, worker_guard) = tracing_appender::non_blocking(std::io::stderr());
        {
            let mut static_guard = WORKER_GUARD.lock().unwrap();
            *static_guard = Some(worker_guard);
        }
        tracing::subscriber::set_global_default(tracing_subscriber::fmt().with_writer(non_blocking).finish()).unwrap();
    }
}
