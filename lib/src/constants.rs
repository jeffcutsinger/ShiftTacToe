use cfg_if::cfg_if;

#[cfg(all(feature = "two_by_two", feature = "three_by_three"))]
compile_error!("features \"two_by_two\" and \"three_by_three\" cannot be enabled at the same time");

cfg_if! {
  if #[cfg(feature = "two_by_two")] {
    pub const ROW_COUNT: u8 = 2;
    pub const COLUMN_COUNT: u8 = 2;
    pub const ROW_COUNT_USIZE: usize = ROW_COUNT as usize;
    pub const COLUMN_COUNT_USIZE: usize = COLUMN_COUNT as usize;
  } else if #[cfg(feature = "three_by_three")] {
    pub const ROW_COUNT: u8 = 3;
    pub const COLUMN_COUNT: u8 = 3;
    pub const ROW_COUNT_USIZE: usize = ROW_COUNT as usize;
    pub const COLUMN_COUNT_USIZE: usize = COLUMN_COUNT as usize;
  } else {
    compile_error!("Either \"two_by_two\" or \"three_by_three\" must be enabled.")
  }
}

pub const PLAY_COUNT: u8 = (ROW_COUNT * 2) + COLUMN_COUNT;
pub const PLAY_COUNT_USIZE: usize = PLAY_COUNT as usize;