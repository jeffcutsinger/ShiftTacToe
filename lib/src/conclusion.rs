// Conclusion represents the the eventual outcome of play between two perfect players.
// If an individual board has a winner, it has a WinState.
// The difference is that there can be no draw on an individual board;
// a pair of perfect players might draw by continuously choosing the same sequence of play.

use crate::player::Player;
use crate::win_state::WinState;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum Conclusion {
    Draw,
    PlayerOneWins,
    PlayerTwoWins,
    PlayersOneAndTwoWin,
}

impl From<Player> for Conclusion {
    fn from(player: Player) -> Self {
        match player {
            Player::One => Self::PlayerOneWins,
            Player::Two => Self::PlayerTwoWins,
        }
    }
}

impl From<WinState> for Conclusion {
    fn from(win_state: WinState) -> Self {
        match win_state {
            WinState::PlayerOne => Self::PlayerOneWins,
            WinState::PlayerTwo => Self::PlayerTwoWins,
            WinState::PlayersOneAndTwo => Self::PlayersOneAndTwoWin,
        }
    }
}

impl Conclusion {
    pub fn choose_best_for_player(player: Player, left: Self, right: Self) -> Self {
        let best_conclusion = Conclusion::from(player);
        match (left, right) {
            (s, _) if s == best_conclusion => best_conclusion,
            (_, s) if s == best_conclusion => best_conclusion,
            // The order of these two clauses is up to interpretation.
            // I'm aiming for games to have a concrete conclusion.
            (_, Self::PlayersOneAndTwoWin) => Self::PlayersOneAndTwoWin,
            (Self::PlayersOneAndTwoWin, _) => Self::PlayersOneAndTwoWin,
            (_, Self::Draw) => Self::Draw,
            (Self::Draw, _) => Self::Draw,
            (s, _) => s
        }
    }

    pub fn coalesce(player: Player, left: Option<Self>, right: Option<Self>) -> Option<Self> {
        let Some(left) = left else { return None; };
        let Some(right) = right else { return None; };
        Some(Self::choose_best_for_player(player, left, right))
    }
}
