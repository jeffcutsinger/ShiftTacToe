use crate::constants::PLAY_COUNT;
use crate::conclusion::Conclusion;
use crate::play::Play;
use crate::player::Player;

use crossbeam::utils::Backoff;

use std::marker::PhantomData;
use std::sync::atomic::{AtomicU8, Ordering};
use std::thread;
use std::time::Duration;

pub trait InvalidValue {
    fn invalid() -> u8;
}

pub struct AtomicStorage<T> where T : From<u8>, u8 : From<T>, T : InvalidValue, T : Default {
    storage: AtomicU8,
    _phantom: PhantomData<T>,
}

impl<T> Default for AtomicStorage<T> where T : From<u8>, u8 : From<T>, T : InvalidValue, T : Default {
    fn default() -> Self {
        Self { storage: AtomicU8::new(T::default().into()), _phantom: Default::default() }
    }
}

impl<T> Clone for AtomicStorage<T> where T : From<u8>, u8 : From<T>, T : InvalidValue, T : Default {
    fn clone(&self) -> Self {
        Self { storage: AtomicU8::new(self.storage.load(Ordering::SeqCst)), _phantom: self._phantom.clone() }
    }
}

impl<T> AtomicStorage<T> where T : From<u8>, u8 : From<T>, T : InvalidValue, T : Default {
    pub fn load(&self) -> T {
        let backoff = Backoff::new();
        loop {
            if let Some(result) = self.try_load() {
                return result.into();
            }

            if backoff.is_completed() {
                thread::sleep(Duration::from_millis(10));
            } else {
                backoff.snooze();
            }
        }
    }

    pub fn try_load(&self) -> Option<T> {
        let maybe_value = self.storage.load(Ordering::SeqCst);
        if maybe_value != T::invalid() {
            Some(maybe_value.into())
        } else {
            None
        }
    }

    pub fn take(&self) -> T {
        let backoff = Backoff::new();
        loop {
            if let Some(result) = self.try_take() {
                return result.into();
            }

            if backoff.is_completed() {
                thread::sleep(Duration::from_millis(10));
            } else {
                backoff.snooze();
            }
        }
    }

    pub fn try_take(&self) -> Option<T> {
        let maybe_value = self.storage.swap(T::invalid(), Ordering::SeqCst);
        if maybe_value != T::invalid() {
            Some(maybe_value.into())
        } else {
            None
        }
    }

    pub fn set(&self, new_value: T) {
        let value = u8::from(new_value);
        assert_ne!(value, T::invalid());
        let result = self.storage.compare_exchange(T::invalid(), value, Ordering::SeqCst, Ordering::SeqCst);
        assert_eq!(result, Ok(T::invalid()));
    }

    pub(crate) fn new(value: T) -> AtomicStorage<T> {
        Self { storage: AtomicU8::new(value.into()), ..Default::default() }
    }

}

fn update_bitmasked_storage(storage: &mut u8, new_value: u8, mask: u8, bit_index: u8) -> u8 {
    let previous_value = bitmasked_value(*storage, mask, bit_index);
    assert_eq!((new_value << bit_index), (new_value << bit_index) & mask, "Value {new_value:08b} does not fit in mask {mask:08b}");
    *storage = (*storage & !mask) | (new_value << bit_index);
    previous_value
}

fn bitmasked_value(storage: u8, mask: u8, bit_index: u8) -> u8 {
    assert_eq!(mask, (mask >> bit_index) << bit_index, "Misaligned mask {mask:08b} and bit_index {bit_index}");
    (storage & mask) >> bit_index
}

pub struct CompactedUntalliedChildrenAndSelfTallied(u8);

impl Default for CompactedUntalliedChildrenAndSelfTallied {
    fn default() -> Self {
        Self(0b000_0_0001)
    }
}

impl InvalidValue for CompactedUntalliedChildrenAndSelfTallied {
    fn invalid() -> u8 {
        0
    }
}

impl From<u8> for CompactedUntalliedChildrenAndSelfTallied {
    fn from(value: u8) -> Self {
        Self(value)
    }
}

impl From<CompactedUntalliedChildrenAndSelfTallied> for u8 {
    fn from(value: CompactedUntalliedChildrenAndSelfTallied) -> Self {
        value.0
    }
}

impl CompactedUntalliedChildrenAndSelfTallied {
    const UNTALLIED_CHILDREN_MASK: u8 = 0b000_0_1111;
    const UNTALLIED_CHILDREN_BIT_INDEX: u8 = 0;
    const TALLIED_MASK: u8 = 0b000_1_0000;
    const TALLIED_BIT_INDEX: u8 = 4;

    pub fn self_tallied(&self) -> bool {
        bitmasked_value(self.0, Self::TALLIED_MASK, Self::TALLIED_BIT_INDEX) == 1
    }

    pub fn set_self_tallied(&mut self) -> u8 {
        update_bitmasked_storage(&mut self.0, 1, Self::TALLIED_MASK, Self::TALLIED_BIT_INDEX)
    }

    fn raw_untallied_children(&self) -> u8 {
        bitmasked_value(self.0, Self::UNTALLIED_CHILDREN_MASK, Self::UNTALLIED_CHILDREN_BIT_INDEX)
    }

    fn set_raw_untallied_children(&mut self, raw_untallied_children: u8) -> u8 {
        assert!(0 < raw_untallied_children && raw_untallied_children <= PLAY_COUNT + 1);
        update_bitmasked_storage(&mut self.0, raw_untallied_children, Self::UNTALLIED_CHILDREN_MASK, Self::UNTALLIED_CHILDREN_BIT_INDEX)
    }

    pub fn untallied_children(&self) -> u8 {
        self.raw_untallied_children() - 1
    }

    pub fn decrement_untallied_children(&mut self) -> Option<u8> {
        let new_raw_untallied_children = self.raw_untallied_children() - 1;
        if new_raw_untallied_children == 0 {
            return None;
        }
        self.set_raw_untallied_children(new_raw_untallied_children);
        Some(new_raw_untallied_children - 1)
    }

    pub fn increment_untallied_children(&mut self) -> Option<u8> {
        let new_raw_untallied_children = self.raw_untallied_children() + 1;
        if new_raw_untallied_children > (PLAY_COUNT + 1) {
            return None;
        }
        self.set_raw_untallied_children(new_raw_untallied_children);
        Some(new_raw_untallied_children - 1)
    }

    pub(crate) fn set_untallied_children(&mut self, new_untallied_children: u8) -> u8 {
        assert!(new_untallied_children <= PLAY_COUNT);
        self.set_raw_untallied_children(new_untallied_children + 1)
    }
}


pub struct CompactedPlayPlayerAndConclusion(u8);

impl Default for CompactedPlayPlayerAndConclusion {
    fn default() -> Self {
        Self(0b0_00_0_0001)
    }
}

impl InvalidValue for CompactedPlayPlayerAndConclusion {
    fn invalid() -> u8 {
        0
    }
}

impl From<u8> for CompactedPlayPlayerAndConclusion {
    fn from(value: u8) -> Self {
        Self(value)
    }
}

impl From<CompactedPlayPlayerAndConclusion> for u8 {
    fn from(value: CompactedPlayPlayerAndConclusion) -> Self {
        value.0
    }
}


impl CompactedPlayPlayerAndConclusion {
    pub fn new(maybe_play: Option<Play>, player: Player, maybe_conclusion: Option<Conclusion>) -> Self {
        let mut result = Self::default();
        if let Some(play) = maybe_play {
            result.set_play(play);
        }
        result.set_player(player);
        if let Some(conclusion) = maybe_conclusion {
            result.set_conclusion(conclusion);
        }
        result
    }

    const PLAY_MASK: u8 = 0b000_0_1111;
    const PLAY_BIT_INDEX: u8 = 0;
    const PLAYER_MASK: u8 = 0b000_1_0000;
    const PLAYER_BIT_INDEX: u8 = 4;
    const CONCLUSION_MASK: u8 = 0b111_0_0000;
    const CONCLUSION_BIT_INDEX: u8 = 5;

    fn compacted_maybe_play(play: Option<Play>) -> u8 {
        match play {
            None => 1,
            Some(Play::ShiftLeft { row: 0 }) => 2,
            Some(Play::ShiftLeft { row: 1 }) => 3,
            Some(Play::ShiftLeft { row: 2 }) => 4,
            Some(Play::ShiftRight { row: 0 }) => 5,
            Some(Play::ShiftRight { row: 1 }) => 6,
            Some(Play::ShiftRight { row: 2 }) => 7,
            Some(Play::DropPiece { column: 0 }) => 8,
            Some(Play::DropPiece { column: 1 }) => 9,
            Some(Play::DropPiece { column: 2 }) => 10,
            _ => panic!("{:?} out of range", play),
        }
    }

    fn expanded_maybe_play(compacted: u8) -> Option<Play> {
        match compacted {
            1 => None,
            2 => Some(Play::ShiftLeft { row: 0 }),
            3 => Some(Play::ShiftLeft { row: 1 }),
            4 => Some(Play::ShiftLeft { row: 2 }),
            5 => Some(Play::ShiftRight { row: 0 }),
            6 => Some(Play::ShiftRight { row: 1 }),
            7 => Some(Play::ShiftRight { row: 2 }),
            8 => Some(Play::DropPiece { column: 0 }),
            9 => Some(Play::DropPiece { column: 1 }),
            10 => Some(Play::DropPiece { column: 2 }),
            _ => panic!("Compacted play out of range: {compacted}"),
        }
    }

    fn compacted_player(player: Player) -> u8 {
        match player {
            Player::One => 0,
            Player::Two => 1,
        }
    }

    fn expanded_player(compacted: u8) -> Player {
        match compacted {
            0 => Player::One,
            1 => Player::Two,
            _ => panic!("Compacted player out of range: {compacted}"),
        }
    }

    fn compacted_maybe_conclusion(maybe_conclusion: Option<Conclusion>) -> u8 {
        match maybe_conclusion {
            None => 0,
            Some(Conclusion::Draw) => 1,
            Some(Conclusion::PlayerOneWins) => 2,
            Some(Conclusion::PlayerTwoWins) => 3,
            Some(Conclusion::PlayersOneAndTwoWin) => 4,
        }
    }

    fn expanded_maybe_conclusion(compacted: u8) -> Option<Conclusion> {
        match compacted {
            0 => None,
            1 => Some(Conclusion::Draw),
            2 => Some(Conclusion::PlayerOneWins),
            3 => Some(Conclusion::PlayerTwoWins),
            4 => Some(Conclusion::PlayersOneAndTwoWin),
            _ => panic!("Compacted conclusion out of range {compacted}"),
        }
    }

    pub fn set_play(&mut self, play: Play) -> Option<Play> {
        Self::expanded_maybe_play(update_bitmasked_storage(&mut self.0, Self::compacted_maybe_play(Some(play)), Self::PLAY_MASK, Self::PLAY_BIT_INDEX))
    }

    pub fn set_player(&mut self, player: Player) -> Player {
        Self::expanded_player(update_bitmasked_storage(&mut self.0, Self::compacted_player(player), Self::PLAYER_MASK, Self::PLAYER_BIT_INDEX))
    }

    pub fn set_conclusion(&mut self, conclusion: Conclusion) -> Option<Conclusion> {
        Self::expanded_maybe_conclusion(update_bitmasked_storage(&mut self.0, Self::compacted_maybe_conclusion(Some(conclusion)), Self::CONCLUSION_MASK, Self::CONCLUSION_BIT_INDEX))
    }

    pub fn play(&self) -> Option<Play> {
        Self::expanded_maybe_play(bitmasked_value(self.0, Self::PLAY_MASK, Self::PLAY_BIT_INDEX))
    }


    pub fn player(&self) -> Player {
        Self::expanded_player(bitmasked_value(self.0, Self::PLAYER_MASK, Self::PLAYER_BIT_INDEX))
    }

    pub fn conclusion(&self) -> Option<Conclusion> {
        Self::expanded_maybe_conclusion(bitmasked_value(self.0, Self::CONCLUSION_MASK, Self::CONCLUSION_BIT_INDEX))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use quickcheck::Arbitrary;

    use tracing::trace;

    #[derive(Clone, Debug)]
    struct UntalliedChildren(u8);
    impl Arbitrary for UntalliedChildren {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            Self(*g.choose((0..=PLAY_COUNT).collect::<Vec<u8>>().as_slice()).unwrap())
        }
    }

    impl Arbitrary for Play {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            *g.choose(Play::all_plays().as_slice()).unwrap()
        }
    }
    impl Arbitrary for Player {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            *g.choose(&[Player::One, Player::Two]).unwrap()
        }
    }
    impl Arbitrary for Conclusion {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            *g.choose(&[Conclusion::Draw, Conclusion::PlayerOneWins, Conclusion::PlayerTwoWins, Conclusion::PlayersOneAndTwoWin]).unwrap()
        }
    }

    #[quickcheck_macros::quickcheck]
    fn property_compacted_play_player_conclusion_preserves_input(play: Option<Play>, player: Player, conclusion: Option<Conclusion>) {
        trace!("property_compacted_play_player_conclusion_preserves_input: {:?}, {:?}, {:?}", play, player, conclusion);
        let compacted = CompactedPlayPlayerAndConclusion::new(play, player, conclusion);
        assert_eq!(compacted.play(), play);
        assert_eq!(compacted.player(), player);
        assert_eq!(compacted.conclusion(), conclusion);
    }

    // Regression test. Conclusion wasn't getting zeroed before update.
    #[quickcheck_macros::quickcheck]
    fn property_compacted_play_player_conclusion_preserves_updated_conclusion(conclusion0: Conclusion, conclusion1: Conclusion) {
        trace!("property_compacted_play_player_conclusion_preserves_updated_conclusion: {:?}, {:?}", conclusion0, conclusion1);
        let mut compacted = CompactedPlayPlayerAndConclusion::default();
        compacted.set_conclusion(conclusion0);
        compacted.set_conclusion(conclusion1);
        assert_eq!(compacted.conclusion(), Some(conclusion1))
    }

    #[quickcheck_macros::quickcheck]
    fn property_compacted_untallied_children_and_self_tallied_preserves_input(untallied_children: UntalliedChildren, self_tallied: bool) {
        trace!("property_compacted_untallied_children_and_self_tallied_preserves_input: {:?}, {:?}", untallied_children.0, self_tallied);
        let mut compacted = CompactedUntalliedChildrenAndSelfTallied::default();
        compacted.set_untallied_children(untallied_children.0);
        if self_tallied {
            compacted.set_self_tallied();
        }
        assert_eq!(compacted.untallied_children(), untallied_children.0);
        assert_eq!(compacted.self_tallied(), self_tallied);

        if untallied_children.0 != 0 {
            compacted.decrement_untallied_children();
            assert_eq!(compacted.untallied_children(), untallied_children.0 - 1);
        }
    }

    #[test]
    fn untallied_children_test() {
        let mut compacted = CompactedUntalliedChildrenAndSelfTallied::default();
        assert_eq!(compacted.untallied_children(), 0);
        for i in 1..=PLAY_COUNT {
            assert_eq!(compacted.increment_untallied_children(), Some(i));
            assert_eq!(compacted.untallied_children(), i);
        }

        assert!(compacted.increment_untallied_children().is_none());
        assert_eq!(compacted.untallied_children(), PLAY_COUNT);

        for i in (0..PLAY_COUNT).rev() {
            assert_eq!(compacted.decrement_untallied_children(), Some(i));
            assert_eq!(compacted.untallied_children(), i);
        }

        assert!(compacted.decrement_untallied_children().is_none());
        assert_eq!(compacted.untallied_children(), 0);
    }

    use crate::position::Position;
    use crate::wld_tree::{WinLoseDrawNode, NodePointerTrait, Diagnostics};

    #[derive(Clone, Default)]
    struct TestPointer(std::sync::Arc<WinLoseDrawNode<TestPointer>>);

    impl std::fmt::Debug for TestPointer {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            let address = std::ptr::NonNull::from(self.0.as_ref());
            f.debug_tuple("TestPointer")
                .field(&address)
                .finish()
        }
    }

    impl NodePointerTrait<TestPointer> for TestPointer {}
    impl AsRef<WinLoseDrawNode<TestPointer>> for TestPointer {
        fn as_ref(&self) -> &WinLoseDrawNode<TestPointer> {
            self.0.as_ref()
        }
    }

    #[test]
    fn conclusion_update_bug_repro() {
        let root_pointer = TestPointer(std::sync::Arc::new(WinLoseDrawNode::default()));
        let mut children_storage: [WinLoseDrawNode<TestPointer>; PLAY_COUNT as usize + 1] = Default::default();
        let mut positions = vec![Position::default()];
        let mut ancestors = vec![root_pointer];
        let plays = [
            Play::DropPiece { column: 0 },
            Play::DropPiece { column: 0 },
            Play::DropPiece { column: 2 },
            Play::DropPiece { column: 1 },
            Play::DropPiece { column: 1 },
            Play::DropPiece { column: 0 },
            Play::DropPiece { column: 1 },
        ];

        let mut last_position = Position::default();
        for play in plays {
            let last_node = ancestors.last().unwrap().clone();
            let position = last_position.make_play(play).unwrap();
            last_node.as_ref().add_child(last_node.clone(), play, &last_position, positions.as_slice(), &mut children_storage);
            if last_node.0.untallied_children() != 1 {
                eprintln!("{:?} had {} untallied children after adding child.", last_node.as_ref(), last_node.0.untallied_children());
                break;
            }
            let node = TestPointer(std::sync::Arc::new(children_storage[0].clone()));

            ancestors.push(node.clone());
            positions.push(position.clone());
            last_position = position.clone();
        }

        let parent_node = ancestors.last().unwrap().clone();
        eprintln!("{:?}", parent_node.as_ref().conclusion());
        let children = parent_node.as_ref().create_children(parent_node.clone(), &last_position, positions.as_slice(), &mut children_storage);
        for child in children {
            if child.conclusion().is_none() {
                continue;
            }
            let child_pointer = TestPointer(std::sync::Arc::new(child.clone()));
            let diagnostics = Diagnostics::default();
            child_pointer.as_ref().tally_recursive(&child_pointer, &diagnostics);
        }
    }
}
