use tracing::trace;

use crate::constants::{COLUMN_COUNT, COLUMN_COUNT_USIZE, ROW_COUNT, ROW_COUNT_USIZE};
use crate::player::Player;
use crate::win_state::{WinState, WinStateExt};

#[derive(Debug, Default, Clone, Copy, Hash, PartialEq, Eq)]
pub struct Grid {
    pub(crate) array: [[Option<Player>; COLUMN_COUNT_USIZE]; ROW_COUNT_USIZE]
}

impl Grid {
    pub fn get(&self, row: u8, column: u8) -> Option<Player> {
        assert!(row < ROW_COUNT);
        assert!(column < COLUMN_COUNT);

        self.array[row as usize][column as usize]
    }

    pub fn shift_left(&mut self, row: u8) {
        assert!(row < ROW_COUNT);
        let row = row as usize;

        for column in 0..COLUMN_COUNT_USIZE {
            let maybe_value_to_right = self.array[row].get(column + 1).and_then(|p| p.clone());
            trace!("Copying {:?} ({row},{}) to ({row},{column}) ", maybe_value_to_right, column + 1);
            self.array[row][column] = maybe_value_to_right;
        }

        self.apply_gravity();
        self.validate().expect(format!("Grid {self:?} is invalid.").as_str());
    }

    pub fn shift_right(&mut self, row: u8) {
        assert!(row < ROW_COUNT);
        let row = row as usize;

        for column in (0..COLUMN_COUNT_USIZE).rev() {
            self.array[row][column] =
                if column != 0 {
                    self.array[row][column - 1]
                } else {
                    None
                };
        }

        self.apply_gravity();
        self.validate().expect("Grid is invalid.");
    }

    pub fn drop_piece(&mut self, column: u8, player: Player) -> bool {
        assert!(column < COLUMN_COUNT);
        let column = column as usize;

        let mut maybe_row_to_set = None;

        for row in (0..ROW_COUNT_USIZE).rev() {
            if self.array[row][column].is_none() {
                maybe_row_to_set = Some(row);
                break;
            }
        }

        if let Some(row_to_set) = maybe_row_to_set {
            self.array[row_to_set][column] = Some(player);
            self.validate().expect("Grid is invalid.");
        }

        maybe_row_to_set.is_some()
    }

    fn apply_gravity(&mut self) {
        trace!("Applying gravity to grid {self:?}");
        for column in 0..COLUMN_COUNT_USIZE {
            trace!("Applying gravity to column {column}");

            for row in (1..ROW_COUNT_USIZE).rev() {
                trace!("Checking if piece {row},{column} is empty");
                if self.array[row][column].is_none() {
                    trace!("Piece {row},{column} is empty");
                    for row_above in (0..row).rev() {
                        assert!(row_above < row);
                        trace!("Checking if piece {row_above},{column} is not empty");
                        if let Some(player) = self.array[row_above][column] {
                            trace!("Piece {row_above},{column} has {:?}", player);
                            self.array[row][column] = Some(player);
                            self.array[row_above][column] = None;
                            break;
                        }
                    }
                }
            }
        }
    }

    pub fn validate(&self) -> Result<(), ()> {
        for column in 0..COLUMN_COUNT_USIZE {
            let mut empty_piece_seen = false;
            for row in (0..ROW_COUNT_USIZE).rev() {
                empty_piece_seen |= self.array[row][column].is_none();
                if empty_piece_seen && self.array[row][column].is_some() {
                    return Err(());
                }
            }
        }
        Ok(())
    }

    pub fn win_state(&self) -> Option<WinState> {
        let mut win_state = None;

        for row in 0..ROW_COUNT {
            let row = row as usize;
            let first_piece = self.array[row][0];
            let mut all_equal = true;
            for column in 1..COLUMN_COUNT {
                let column = column as usize;
                all_equal &= first_piece == self.array[row][column];
            }
            if let Some(player) = first_piece.filter(|_| all_equal) {
                win_state = Some(win_state.with_player_winning(player));
            }
        }

        for column in 0..COLUMN_COUNT {
            let column = column as usize;
            let first_piece = self.array[0][column];
            let mut all_equal = true;
            for row in 1..ROW_COUNT {
                let row = row as usize;
                all_equal &= first_piece == self.array[row][column];
            }
            if let Some(player) = first_piece.filter(|_| all_equal) {
                win_state = Some(win_state.with_player_winning(player));
            }
        }

        if ROW_COUNT != COLUMN_COUNT {
            unimplemented!("Having unequal ROW_COUNT and COLUMN_COUNT is not implemented");
        }

        let top_left_piece = self.array[0][0];
        let mut all_down_slope_equal = true;
        for row_column in 1..ROW_COUNT {
            let row_column = row_column as usize;
            all_down_slope_equal &= top_left_piece == self.array[row_column][row_column];
        }
        if let Some(player) = top_left_piece.filter(|_| all_down_slope_equal) {
            win_state = Some(win_state.with_player_winning(player));
        }

        let bottom_left_piece = self.array[ROW_COUNT as usize - 1][0];
        let mut all_up_slope_equal = true;
        for row in 0..(ROW_COUNT-1) {
            let row = row as usize;
            let column = COLUMN_COUNT as usize - row - 1;
            all_up_slope_equal &= bottom_left_piece == self.array[row][column];
        }
        if let Some(player) = bottom_left_piece.filter(|_| all_up_slope_equal) {
            win_state = Some(win_state.with_player_winning(player));
        }

        win_state
    }
}

#[cfg(all(test, feature="two_by_two"))]
mod tests {
    use super::*;

    #[test]
    fn win_states() {
        assert_eq!(Grid { array: [[Some(Player::One), Some(Player::One)], [None, None]] }.win_state(), Some(WinState::PlayerOne));
        assert_eq!(Grid { array: [[None, None], [Some(Player::One), Some(Player::One)]] }.win_state(), Some(WinState::PlayerOne));
        assert_eq!(Grid { array: [[Some(Player::Two), Some(Player::Two)], [None, None]] }.win_state(), Some(WinState::PlayerTwo));
        assert_eq!(Grid { array: [[None, None], [Some(Player::Two), Some(Player::Two)]] }.win_state(), Some(WinState::PlayerTwo));

        assert_eq!(Grid { array: [[Some(Player::One), None], [Some(Player::One), None]] }.win_state(), Some(WinState::PlayerOne));
        assert_eq!(Grid { array: [[None, Some(Player::One)], [None, Some(Player::One)]] }.win_state(), Some(WinState::PlayerOne));
        assert_eq!(Grid { array: [[Some(Player::Two), None], [Some(Player::Two), None]] }.win_state(), Some(WinState::PlayerTwo));
        assert_eq!(Grid { array: [[None, Some(Player::Two)], [None, Some(Player::Two)]] }.win_state(), Some(WinState::PlayerTwo));

        assert_eq!(Grid { array: [[Some(Player::One), None], [None, Some(Player::One)]] }.win_state(), Some(WinState::PlayerOne));
        assert_eq!(Grid { array: [[None, Some(Player::One)], [Some(Player::One), None]] }.win_state(), Some(WinState::PlayerOne));
        assert_eq!(Grid { array: [[Some(Player::Two), None], [None, Some(Player::Two)]] }.win_state(), Some(WinState::PlayerTwo));
        assert_eq!(Grid { array: [[None, Some(Player::Two)], [Some(Player::Two), None]] }.win_state(), Some(WinState::PlayerTwo));
    }
}


#[cfg(all(test, feature="three_by_three"))]
mod tests {
    use super::*;

    use quickcheck::Arbitrary;
    use quickcheck_macros::quickcheck;

    use std::sync::OnceLock;

    #[derive(Clone, Copy, Debug)]
    struct Row(u8);
    impl Arbitrary for Row {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            let rows = (0..ROW_COUNT).map(|row| Row(row)).collect::<Vec<Row>>();
            *g.choose(rows.as_slice()).unwrap()
        }
    }


    const ALL_BASIC_WIN_STATES: [[[u8; ROW_COUNT_USIZE]; COLUMN_COUNT_USIZE]; ROW_COUNT_USIZE + COLUMN_COUNT_USIZE + 2] = [
        [[1, 1, 1], [0, 0, 0], [0, 0, 0]],
        [[0, 0, 0], [1, 1, 1], [0, 0, 0]],
        [[0, 0, 0], [0, 0, 0], [1, 1, 1]],

        [[1, 0, 0], [1, 0, 0], [1, 0, 0]],
        [[0, 1, 0], [0, 1, 0], [0, 1, 0]],
        [[0, 0, 1], [0, 0, 1], [0, 0, 1]],

        [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
        [[0, 0, 1], [0, 1, 0], [1, 0, 0]],
    ];

    fn create_all_basic_win_state_grids() -> [(Grid, Player); (ROW_COUNT_USIZE + COLUMN_COUNT_USIZE + 2) * 2] {
        let mut result = [(Grid::default(), Player::One); (ROW_COUNT_USIZE + COLUMN_COUNT_USIZE + 2) * 2];
        let mut result_index = 0;
        for player in [Player::One, Player::Two] {
            for basic_win_state in ALL_BASIC_WIN_STATES {
                assert!(result_index < result.len());
                for row in 0..ROW_COUNT_USIZE {
                    for column in 0..COLUMN_COUNT_USIZE {
                        result[result_index].0.array[row][column] = if basic_win_state[row][column] == 1 { Some(player) } else { None };
                        result[result_index].1 = player;
                    }
                }
                result_index += 1;
            }
        }
        result
    }

    impl Arbitrary for Grid {
        fn arbitrary(g: &mut quickcheck::Gen) -> Self {
            let mut result = Grid::default();
            loop {
                for row in 0..ROW_COUNT_USIZE {
                    for column in 0..COLUMN_COUNT_USIZE {
                        result.array[row][column] = g.choose(&[None, Some(Player::One), Some(Player::Two)]).unwrap().clone()
                    }
                }
                if result.validate().is_ok() {
                    break;
                }
            }
            result
        }
    }

    #[test]
    fn basic_win_states_win_for_the_correct_player() {
        for basic_win_state_grid in all_basic_win_state_grids() {
            assert_eq!(basic_win_state_grid.0.win_state(), Some(basic_win_state_grid.1.into()))
        }
    }

    #[test]
    fn up_slope_regression() {
        assert_eq!(
            Grid {
                array: [
                    [None, None, None],
                    [None, Some(Player::One), None],
                    [Some(Player::One), None, None],
                ]
            }.win_state(),
            None
        );
    }

    static ALL_BASIC_WIN_STATE_GRIDS: OnceLock<[(Grid, Player); (ROW_COUNT_USIZE + COLUMN_COUNT_USIZE + 2) * 2]> = OnceLock::new();
    fn all_basic_win_state_grids() -> &'static [(Grid, Player); (ROW_COUNT_USIZE + COLUMN_COUNT_USIZE + 2) * 2] {
        ALL_BASIC_WIN_STATE_GRIDS.get_or_init(create_all_basic_win_state_grids)
    }

    #[quickcheck]
    fn property_win_states_match_basic_win_state_grids(grid: Grid) {
        let mut matching_grids = vec![];

        for win_state_grid in all_basic_win_state_grids() {
            let mut matches_grid = true;
            for row in 0..ROW_COUNT_USIZE {
                for column in 0..COLUMN_COUNT_USIZE {
                    if let Some(player) = win_state_grid.0.array[row][column] {
                        matches_grid &= grid.array[row][column] == Some(player);
                        if !matches_grid {
                            break;
                        }
                    }
                }
                if !matches_grid {
                    break;
                }
            }
            if matches_grid {
                matching_grids.push(win_state_grid);
            }
        }

        let mut final_win_state = None;
        for win_state in matching_grids {
            final_win_state = Some(final_win_state.with_player_winning(win_state.1));
        }
        assert_eq!(final_win_state, grid.win_state());
    }

    #[quickcheck]
    fn property_left_shift_maintains_validity(mut grid: Grid, row: Row) {
        grid.shift_left(row.0);
        assert!(grid.validate().is_ok());
    }
}