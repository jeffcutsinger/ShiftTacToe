use std::error::Error;
use std::sync::Arc;

use tracing;

pub trait ThreadSubscriberFactory {
    fn create(&self, thread_type: &str, thread_number: usize) -> Result<Arc<dyn tracing::Subscriber + Send + Sync>, Box<dyn Error>>;
}

