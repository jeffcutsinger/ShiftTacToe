#[derive(Debug, Default, Clone, Copy, Hash, PartialEq, Eq)]
pub enum RowPosition {
    Left,
    #[default]
    Center,
    Right
}

impl RowPosition {
    pub fn shift_left(self) -> Option<Self> {
        match self {
            Self::Left => None,
            Self::Center => Some(Self::Left),
            Self::Right => Some(Self::Center),
        }
    }

    pub fn shift_right(self) -> Option<Self> {
        match self {
            Self::Left => Some(Self::Center),
            Self::Center => Some(Self::Right),
            Self::Right => None,
        }
    }
}
