use crate::ThreadSubscriberFactory;
use crate::compact::{AtomicStorage, CompactedPlayPlayerAndConclusion, CompactedUntalliedChildrenAndSelfTallied};
use crate::conclusion::Conclusion;
use crate::constants::PLAY_COUNT_USIZE;
use crate::slap::{LockedSlap, Slap, Shard};
use crate::play::Play;
use crate::player::Player;
use crate::position::Position;

use crossterm::{execute, cursor, style};

use crossbeam::utils::Backoff;

use tracing::{debug, info, instrument, trace, warn, info_span};

use std::ops::DerefMut;
use std::ptr::NonNull;
use std::sync::{Arc, Mutex};
use std::sync::atomic::{AtomicBool, AtomicUsize, Ordering};
use std::thread::JoinHandle;
use std::{thread, mem};
use std::time::Duration;

pub trait NodePointerTrait<TSelf> : Clone + AsRef<WinLoseDrawNode<TSelf>> + std::fmt::Debug where TSelf : NodePointerTrait<TSelf> {
    fn parent(&self) -> &Option<TSelf> {
        &self.as_ref().parent
    }
}

#[derive(Clone)]
pub struct WinLoseDrawNode<TPointer> where TPointer : NodePointerTrait<TPointer> {
    parent: Option<TPointer>,
    untallied_children_and_self_tallied_storage: AtomicStorage<CompactedUntalliedChildrenAndSelfTallied>,
    play_player_and_conclusion_storage: AtomicStorage<CompactedPlayPlayerAndConclusion>,
    sentinel: u32,
}

impl<TPointer> Default for WinLoseDrawNode<TPointer> where TPointer : NodePointerTrait<TPointer> {
    fn default() -> Self {
        Self {
            parent: None,
            untallied_children_and_self_tallied_storage: Default::default(),
            play_player_and_conclusion_storage: Default::default(),
            sentinel: 0x13579BCE,
        }
    }
}

impl<TPointer> WinLoseDrawNode<TPointer> where TPointer : NodePointerTrait<TPointer> {
    pub(crate) fn new(parent_pointer: TPointer, play: Play, position: Position, ancestor_positions: &[Position]) -> Self {
        let conclusion = position.win_state()
            .map(Conclusion::from)
            .or_else(|| if ancestor_positions.contains(&position) { Some(Conclusion::Draw) } else { None });
        let play_player_and_conclusion = CompactedPlayPlayerAndConclusion::new(Some(play), position.player(), conclusion);

        let result = Self {
            parent: Some(parent_pointer),
            play_player_and_conclusion_storage: AtomicStorage::new(play_player_and_conclusion),
            ..Default::default()
        };
        result
    }

    fn validate_sentinel(&self) {
        assert_eq!(self.sentinel, 0x13579BCE);
    }

    pub fn untallied_children(&self) -> u8 {
        self.validate_sentinel();
        self.untallied_children_and_self_tallied_storage.load().untallied_children()
    }

    pub fn is_tallied(&self) -> bool {
        self.validate_sentinel();
        self.untallied_children_and_self_tallied_storage.load().self_tallied()
    }

    pub fn as_stats_string(&self) -> String {
        self.validate_sentinel();
        let tallies = self.untallied_children_and_self_tallied_storage.load();
        let play_player_and_conclusion = self.play_player_and_conclusion_storage.load();
        format!(
            "{{untallied_children: {}, tallied: {}, play: {:?}, player: {:?}, conclusion: {:?}}}",
            tallies.untallied_children(),
            tallies.self_tallied(),
            play_player_and_conclusion.play(),
            play_player_and_conclusion.player(),
            play_player_and_conclusion.conclusion(),
        )
    }

    pub fn conclusion(&self) -> Option<Conclusion> {
        self.validate_sentinel();
        self.play_player_and_conclusion_storage.load().conclusion()
    }

    pub fn set_conclusion(&self, conclusion: Conclusion) {
        self.validate_sentinel();
        let mut play_player_and_conclusion = self.play_player_and_conclusion_storage.take();
        play_player_and_conclusion.set_conclusion(conclusion);
        self.play_player_and_conclusion_storage.set(play_player_and_conclusion);
    }

    pub fn play(&self) -> Option<Play> {
        self.validate_sentinel();
        self.play_player_and_conclusion_storage.load().play()
    }

    pub fn player(&self) -> Player {
        self.validate_sentinel();
        self.play_player_and_conclusion_storage.load().player()
    }

    pub fn parent(&self) -> Option<TPointer> {
        self.validate_sentinel();
        self.parent.clone()
    }

    pub fn depth(&self) -> usize {
        self.validate_sentinel();
        let mut depth = 1;
        let mut current = match self.parent() {
            Some(p) => p,
            None => return 0,
        };

        while let Some(parent) = current.parent() {
            depth += 1;
            current = parent.clone();
        }

        depth
    }

    pub(crate) fn tally_recursive(&self, this: &TPointer, diagnostics: &Diagnostics) {
        self.validate_sentinel();
        let mut maybe_current_node_pointer = self.tally_individual(this, diagnostics);
        while let Some(current_node_pointer) = maybe_current_node_pointer {
            trace!("TALLY: Tallying node {:?} {:?}", &current_node_pointer, current_node_pointer.as_ref());
            maybe_current_node_pointer = current_node_pointer.as_ref().tally_individual(&current_node_pointer, diagnostics);
        }
    }

    pub(crate) fn tally_individual(&self, this: &TPointer, diagnostics: &Diagnostics) -> Option<TPointer> {
        self.validate_sentinel();
        trace!("TALLY: Tallying node {:?} {:?}", this, self);

        let mut self_tallies = self.untallied_children_and_self_tallied_storage.take();
        if self_tallies.untallied_children() != 0 {
            trace!("TALLY: Node still has untallied children.");
            self.untallied_children_and_self_tallied_storage.set(self_tallies);
            return None;
        }
        if self_tallies.self_tallied() {
            trace!("TALLY: Node already tallied.");
            self.untallied_children_and_self_tallied_storage.set(self_tallies);
            return None;
        } else {
            trace!("TALLY: Node not tallied; setting as tallied.");
            self_tallies.set_self_tallied();
            diagnostics.increment_tallied_count();
            self.untallied_children_and_self_tallied_storage.set(self_tallies);
        }

        let Some(parent_pointer) = self.parent() else {
            trace!("TALLY: no parent node");
            let mut self_tallies = self.untallied_children_and_self_tallied_storage.take();
            self_tallies.set_self_tallied();
            diagnostics.increment_tallied_count();
            self.untallied_children_and_self_tallied_storage.set(self_tallies);
            return None;
        };
        let parent_node = parent_pointer.as_ref();

        let self_conclusion = self.conclusion();

        let mut parent_play_player_and_conclusion = parent_node.play_player_and_conclusion_storage.take();

        let current_parent_conclusion = parent_play_player_and_conclusion.conclusion();
        let parent_player = parent_play_player_and_conclusion.player();
        let new_parent_conclusion = Conclusion::coalesce(parent_player, current_parent_conclusion, self_conclusion);
        if new_parent_conclusion != current_parent_conclusion {
            trace!("TALLY: updating {:?} conclusion to {:?}", &parent_pointer, new_parent_conclusion);
        }

        if let Some(new_parent_conclusion) = new_parent_conclusion {
            parent_play_player_and_conclusion.set_conclusion(new_parent_conclusion);
        }
        parent_node.play_player_and_conclusion_storage.set(parent_play_player_and_conclusion);

        let mut parent_tallies = parent_node.untallied_children_and_self_tallied_storage.take();
        let Some(new_parent_untallied_children) = parent_tallies.decrement_untallied_children() else {
            panic!("Tallying a child, but parent_node's untallied_children was 0. Child: {self:?}, parent: {parent_node:?}");
        };
        trace!("TALLY: {:?} new untallied children: {}", &parent_pointer, new_parent_untallied_children);

        parent_node.untallied_children_and_self_tallied_storage.set(parent_tallies);
        if new_parent_untallied_children == 0 {
            trace!("TALLY: {:?} has no untallied children, returning to be tallied.", &parent_pointer);
            Some(parent_pointer)
        } else {
            None
        }
    }

    pub fn get_ancestors(&self, output: &mut Vec<TPointer>) {
        self.validate_sentinel();
        trace!("NODE: getting ancestors for {:?}", self.as_stats_string());
        let Some(initial_ref) = self.parent() else {
            trace!("NODE: at root node, returning");
            return;
        };

        let mut current_pointer = initial_ref;
        loop {
            output.push(current_pointer.clone());
            let parent_pointer: TPointer;
            {
                let current_ref = current_pointer.as_ref();
                trace!("NODE: getting ancestors for {:?}", &current_ref);
                parent_pointer = match current_ref.parent() {
                    Some(r) => r,
                    None => {
                        trace!("NODE: at root node, returning");
                        break;
                    }
                };
            }
            current_pointer = parent_pointer;
        }

    }

    pub fn get_positions(&self, output: &mut Vec<Position>, ancestors: &[TPointer]) {
        self.validate_sentinel();
        output.clear();
        let mut position = Position::default();
        output.push(position.clone());
        trace!("NODE: getting positions. Starting with default.\n{}", position.as_2d_string());
        for ancestor_pointer in ancestors.iter().rev().skip(1) {
            let ancestor_ref = ancestor_pointer.as_ref();
            let play = ancestor_ref.play().expect("Non-root node had no play.");
            position = position.make_play(play).expect(&format!("Replaying ancestor nodes resulted in invalid play:\n{}{:?}", position.as_2d_string(), play));
            trace!("NODE: play {:?} results in position\n{}", play, position.as_2d_string());
            output.push(position.clone());
        }

        if let Some(play) = self.play() {
            position = position.make_play(play).expect(&format!("Replaying ancestor nodes resulted in invalid play:\n{}{:?}", position.as_2d_string(), play));
            trace!("NODE: play {:?} results in position\n{}", play, position.as_2d_string());
            output.push(position.clone());
        }
        trace!("NODE: finished replay.");
    }

    pub fn create_children<'storage>(&self, self_pointer: TPointer, current_position: &Position, position_history: &[Position], output_storage: &'storage mut [WinLoseDrawNode<TPointer>; PLAY_COUNT_USIZE + 1]) -> &'storage [WinLoseDrawNode<TPointer>] {
        self.validate_sentinel();
        let mut child_count = 0;
        for play in Play::all_plays() {
            let play = *play;

            if self.add_child(self_pointer.clone(), play, current_position, position_history, output_storage) {
                child_count += 1;
            }
        }

        let untallied_children = self.untallied_children();

        trace!("Created children for {self_pointer:?}, number created: {child_count}, untallied: {untallied_children}");

        assert!(untallied_children == child_count || self.conclusion().is_some());
        assert!(current_position.win_state() == None);

        &output_storage[0..untallied_children as usize]
    }

    pub fn add_child<'storage>(&self, self_pointer: TPointer, play: Play, current_position: &Position, position_history: &[Position], output_storage: &'storage mut [WinLoseDrawNode<TPointer>; PLAY_COUNT_USIZE + 1]) -> bool {
        self.validate_sentinel();
        let Ok(child_position) = current_position.make_play(play) else {
            trace!("{play:?} would be invalid");
            return false;
        };

        let mut play_player_and_conclusion = self.play_player_and_conclusion_storage.take();
        if play_player_and_conclusion.conclusion().is_some() {
            // Branch has been pruned.
            self.play_player_and_conclusion_storage.set(play_player_and_conclusion);
            return false;
        }
        let mut untallied_children_and_self_tallied = self.untallied_children_and_self_tallied_storage.take();

        let child_node = WinLoseDrawNode::new(self_pointer.clone(), play, child_position.clone(), position_history);
        assert!(child_position.win_state().map(Conclusion::from) == child_node.conclusion() || child_node.conclusion() == Some(Conclusion::Draw));
        assert_ne!(child_position.player(), current_position.player());
        trace!("child node {child_node:?} created.");
        if child_node.conclusion() == Some(current_position.player().into()) {
            trace!("child node wins for current player. Pruning branch.");
            untallied_children_and_self_tallied.set_untallied_children(0);
            output_storage[0] = child_node;
            play_player_and_conclusion.set_conclusion(current_position.player().into());
            self.play_player_and_conclusion_storage.set(play_player_and_conclusion);
            self.untallied_children_and_self_tallied_storage.set(untallied_children_and_self_tallied);
            return false;
        }

        self.play_player_and_conclusion_storage.set(play_player_and_conclusion);
        output_storage[untallied_children_and_self_tallied.untallied_children() as usize] = child_node;
        let new_tally = untallied_children_and_self_tallied.increment_untallied_children();
        assert!(new_tally.is_some());
        self.untallied_children_and_self_tallied_storage.set(untallied_children_and_self_tallied);

        true
    }
}

impl<TPointer> std::fmt::Debug for WinLoseDrawNode<TPointer> where TPointer : NodePointerTrait<TPointer> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.validate_sentinel();
        let maybe_tallies = self.untallied_children_and_self_tallied_storage.try_load();
        let maybe_other = self.play_player_and_conclusion_storage.try_load();
        f.debug_struct("WinLoseDrawNode")
            .field("parent", &self.parent.as_ref().map(|p| p))
            .field("untallied_children", &maybe_tallies.as_ref().map(|t| t.untallied_children().to_string()).unwrap_or("(locked)".to_string()))
            .field("self_tallied", &maybe_tallies.as_ref().map(|t| t.self_tallied().to_string()).unwrap_or("(locked)".to_string()))
            .field("play", &maybe_other.as_ref().map(|o| format!("{:?}", o.play())).unwrap_or("(locked)".to_string()))
            .field("player", &maybe_other.as_ref().map(|o| format!("{:?}", o.player())).unwrap_or("(locked)".to_string()))
            .field("conclusion", &maybe_other.as_ref().map(|o| format!("{:?}", o.conclusion())).unwrap_or("(locked)".to_string()))
            .finish()
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct NodePointer(NonNull<WinLoseDrawNode<NodePointer>>);
unsafe impl Sync for NodePointer {}
unsafe impl Send for NodePointer {}

impl From<NonNull<WinLoseDrawNode<NodePointer>>> for NodePointer {
    fn from(value: NonNull<WinLoseDrawNode<NodePointer>>) -> Self {
        Self(value)
    }
}

impl NodePointerTrait<NodePointer> for NodePointer {}

impl AsRef<WinLoseDrawNode<NodePointer>> for NodePointer {
    fn as_ref(&self) -> &WinLoseDrawNode<Self> {
        unsafe { self.0.as_ref() }
    }
}

impl std::ops::Deref for NodePointer {
    type Target = WinLoseDrawNode<Self>;

    fn deref(&self) -> &Self::Target {
        unsafe { self.0.as_ref() }
    }
}

struct AddNodeResult { is_leaf: bool, node_pointer: NodePointer }

impl AddNodeResult {
    fn is_leaf(&self) -> bool {
        self.is_leaf
    }

    fn node_pointer(&self) -> &NodePointer {
        &self.node_pointer
    }
}

pub struct ConstructionParameters {
    approximate_number_threads: usize,
    chunk_size: usize,
    max_depth: usize,
    thread_subscriber_factory: Arc<dyn ThreadSubscriberFactory>
}

impl ConstructionParameters {
    pub fn new<TThreadSubscriberFactory : ThreadSubscriberFactory + 'static>(approximate_number_threads: usize, chunk_size: usize, max_depth: usize, thread_subscriber_factory: TThreadSubscriberFactory) -> Self {
        Self { approximate_number_threads, chunk_size, max_depth, thread_subscriber_factory: Arc::new(thread_subscriber_factory) }
    }
}

pub struct ConstructionCoordinator {
    queue_disabled: bool,
    queue: crossbeam::queue::ArrayQueue<NodePointer>,
    constructor_count: usize,
    started_constructors: AtomicUsize,
    finished_constructors: AtomicUsize,
    tallier_count: usize,
    started_talliers: AtomicUsize,
    finished_talliers: AtomicUsize,
}

impl ConstructionCoordinator {
    pub fn new(constructor_count: usize, disable_queue: bool) -> Arc<Self> {
        Arc::new(Self {
            queue_disabled: disable_queue,
            queue: crossbeam::queue::ArrayQueue::new(constructor_count * 4),
            constructor_count,
            started_constructors: AtomicUsize::new(0),
            finished_constructors: AtomicUsize::new(0),
            tallier_count: constructor_count,
            started_talliers: AtomicUsize::new(0),
            finished_talliers: AtomicUsize::new(0),
        })
    }

    pub fn constructor_count(&self) -> usize {
        self.constructor_count
    }


    pub fn starting_construction(&self) {
        let new_count = self.started_constructors.fetch_add(1, Ordering::AcqRel) + 1;
        assert!(new_count <= self.constructor_count, "More constructors started {new_count} than total constructor count {}", self.constructor_count);
    }

    pub fn finished_constructing(&self) {
        let new_count = self.finished_constructors.fetch_add(1, Ordering::AcqRel) + 1;
        assert!(new_count <= self.constructor_count, "More constructors finished {new_count} than total constructor count {}", self.constructor_count);
    }

    pub fn tallier_count(&self) -> usize {
        self.tallier_count
    }

    pub fn starting_tallying(&self) {
        let new_count = self.started_talliers.fetch_add(1, Ordering::AcqRel) + 1;
        assert!(new_count <= self.tallier_count, "More talliers started {new_count} than total tallier count {}", self.tallier_count);
    }

    pub fn finished_tallying(&self) {
        let new_count = self.finished_talliers.fetch_add(1, Ordering::AcqRel) + 1;
        assert!(new_count <= self.tallier_count, "More talliers started {new_count} than total tallier count {}", self.tallier_count);
    }

    // during startup this can under-count, otherwise it can over-count.
    pub fn approximate_active_constructor_count(&self) -> usize {
        let started = self.started_constructors.load(Ordering::Acquire);
        if started != self.constructor_count {
            return started;
        }
        return self.constructor_count - self.finished_constructors.load(Ordering::Acquire);
    }

    //same as above
    pub fn approximate_active_tallier_count(&self) -> usize {
        let started = self.started_talliers.load(Ordering::Acquire);
        if started != self.tallier_count {
            return started;
        }
        return self.tallier_count - self.finished_talliers.load(Ordering::Acquire);
    }

    pub fn node_constructed(&self, mut pointer: NodePointer) {
        if self.queue_disabled {
            return;
        }
        let backoff = Backoff::new();
        trace!("Enqueueing node {pointer:?}");
        while let Err(p) = self.queue.push(pointer) {
            pointer = p;
            if backoff.is_completed() {
                thread::sleep(Duration::from_millis(10));
            } else {
                backoff.snooze();
            }
        }
    }

    pub fn get_next_constructed_node(&self) -> Option<NodePointer> {
        if self.queue_disabled {
            return None;
        }

        let backoff = Backoff::new();
        loop {
            if let node_pointer @ Some(_) = self.queue.pop() {
                return node_pointer;
            }

            if backoff.is_completed() {
                thread::sleep(Duration::from_millis(10));
            } else {
                backoff.snooze();
            }

            if self.finished_constructors.load(Ordering::Acquire) == self.constructor_count && self.queue.is_empty() {
                return None;
            }
        }
    }
}

pub struct WinLoseDrawTree {
    nodes: Slap<WinLoseDrawNode<NodePointer>>,
    root_node: Mutex<Option<NodePointer>>,
    diagnostics: Arc<Diagnostics>,
}

impl WinLoseDrawTree {
    pub fn build(parameters: &ConstructionParameters) -> Arc<Self> {
        let diagnostics_handle = DiagnosticsHandle::new();

        let approximate_num_threads = parameters.approximate_number_threads;
        let default_chunk_size = parameters.chunk_size;
        let max_depth = parameters.max_depth;

        trace!(task = "construction", "Constructing WLD tree. approximate_num_threads: {approximate_num_threads}, default_chunk_size: {default_chunk_size}, max_depth: {max_depth}");
        let result = Arc::new(Self {
            nodes: Slap::new(approximate_num_threads * 2, default_chunk_size),
            root_node: Mutex::new(None),
            diagnostics: diagnostics_handle.state_clone(),
        });

        let (root_shard, root_node, roots) = result.construct_root_shard(parameters);
        result.set_root_node(root_node);
        result.nodes.release_shard(root_shard);
        let coordinator = ConstructionCoordinator::new(roots.len(), false);

        let mut construction_threads = Self::construct(result.clone(), roots, parameters, coordinator.clone());
        let mut tallier_threads = Self::tally_wins(result.clone(), parameters.thread_subscriber_factory.clone(), coordinator.clone());

        let mut old_active_constructors = 0;
        let mut old_active_talliers = 0;
        loop {
            let active_constructors = coordinator.approximate_active_constructor_count();
            let active_talliers = coordinator.approximate_active_tallier_count();
            if old_active_talliers != active_talliers || old_active_constructors != active_constructors {
                info!("TALLY: Active constructors: {active_constructors}, active talliers: {active_talliers}");
                old_active_constructors = active_constructors;
                old_active_talliers = active_talliers;
            }
            if active_talliers == 0 && active_constructors == 0 {
                break;
            }
            thread::sleep(Duration::from_secs(1));
        }
        debug!("Waiting for {} threads to finish.", construction_threads.len() + tallier_threads.len());
        for join_handle in construction_threads.drain(..).chain(tallier_threads.drain(..)) {
            let thread_name = join_handle.thread().name().map(|s| s.to_string());
            let backoff = Backoff::new();
            while !join_handle.is_finished() && !backoff.is_completed() {
                backoff.snooze();
            }
            if !join_handle.is_finished() {
                warn!("No active constructors or talliers, but thread {thread_name:?} not finished");
            }
            join_handle.join().unwrap();
        }

        diagnostics_handle.finished();
        result
    }

    pub fn lock_nodes<'this>(&'this self) -> LockedSlap<'this, WinLoseDrawNode<NodePointer>> {
        self.nodes.lock()
    }

    pub fn node_count(&self) -> usize {
        self.nodes.node_count()
    }

    fn set_root_node(&self, root_node_ptr: NodePointer) {
        let mut root_node = self.root_node.lock().unwrap();
        let _ = root_node.deref_mut().insert(root_node_ptr);
    }

    pub fn root_node(&self) -> Option<NodePointer> {
        self.root_node.lock().unwrap().as_ref().cloned()
    }

    fn add_node(&self, shard: &mut Shard<WinLoseDrawNode<NodePointer>>, node: WinLoseDrawNode<NodePointer>) -> AddNodeResult {
        let is_leaf = node.conclusion().is_some();
        let node_pointer = NodePointer(shard.push(node));
        self.diagnostics.increment_node_count();

        AddNodeResult { node_pointer, is_leaf }
    }

    #[instrument(skip_all)]
    fn construct(this: Arc<Self>, roots: Vec<NodePointer>, parameters: &ConstructionParameters, coordinator: Arc<ConstructionCoordinator>) -> Vec<JoinHandle<()>> {
        let max_depth = parameters.max_depth;

        let mut join_handles = Vec::with_capacity(roots.len());
        for root in roots {
            assert!(root.as_ref().conclusion().is_none());
            let thread_name = format!("constructor-{}-{:?}", join_handles.len(), root);
            trace!(task = "construction", "Creating construction thread {} with root node {:?}", join_handles.len(), root.as_ref());
            let this = this.clone();
            let builder = thread::Builder::new().name(thread_name);
            let subscriber = parameters.thread_subscriber_factory.create("constructor", join_handles.len()).unwrap();
            let coordinator = coordinator.clone();
            join_handles.push(builder.spawn(move || {
                tracing::subscriber::with_default(subscriber, move || {
                    coordinator.starting_construction();
                    let mut shard = this.nodes.create_shard_and_take(0);
                    this.construct_recursive(root, &mut shard, max_depth, coordinator.clone());
                    this.nodes.release_shard(shard);
                    coordinator.finished_constructing();
                });
            }).unwrap())
        }

        join_handles
    }

    fn construct_root_shard(&self, parameters: &ConstructionParameters) -> (Shard<WinLoseDrawNode<NodePointer>>, NodePointer, Vec<NodePointer>) {
        trace!("constructing root shard.");

        let mut root_shard = Shard::new(if parameters.approximate_number_threads == 1 { parameters.chunk_size } else { parameters.approximate_number_threads * 8 });
        let root_node = WinLoseDrawNode::default();
        let mut root_node_ptr = NodePointer(root_shard.push(root_node.clone()));
        let mut roots = vec![];

        let mut depth = 0;
        while root_shard.len() < parameters.approximate_number_threads {
            roots.clear();
            self.diagnostics.clear_node_count();
            depth += 1;

            trace!("looking for roots of depth {depth}");

            root_shard = Shard::new(if parameters.approximate_number_threads == 1 { parameters.chunk_size } else { parameters.approximate_number_threads * 8 });
            root_node_ptr = NodePointer(root_shard.push(root_node.clone()));
            self.diagnostics.increment_node_count();

            let dummy_coordinator = ConstructionCoordinator::new(1, true);

            self.construct_recursive(root_node_ptr.clone(), &mut root_shard, depth, dummy_coordinator.clone());

            trace!("Finding roots (nodes with no conclusion and depth {depth}");
            let mut index = 0;
            while let Some(node_ptr) = root_shard.get(index) {
                let node_ptr = NodePointer(node_ptr);
                let node = node_ptr.as_ref();
                let node_conclusion = node.conclusion();
                let node_depth = node.depth();
                trace!("Looking at node index {index} ({:?}). Conclusion: {:?}, depth: {:?}", node, node_conclusion, node_depth);
                if node.conclusion().is_none() && node.depth() == depth {
                    trace!("Node is root.");
                    roots.push(node_ptr);
                }
                index += 1;
            }
            assert!(!roots.is_empty());

            trace!("Done constructing for depth {depth}. New root shard length: {}.", root_shard.len());
        }

        trace!("Finished construction root shard. Root shard length: {}, root count: {}, root_node_ptr: {root_node_ptr:?}", root_shard.len(), roots.len());

        (root_shard, root_node_ptr, roots)
    }

    fn construct_recursive(&self, root_node_ptr: NodePointer, shard: &mut Shard<WinLoseDrawNode<NodePointer>>, max_depth: usize, coordinator: Arc<ConstructionCoordinator>) {
        let mut node_stack = vec![root_node_ptr];
        let mut ancestors = vec![];
        let mut positions = vec![];
        let mut current_node_children_storage: [WinLoseDrawNode<NodePointer>; PLAY_COUNT_USIZE + 1] = Default::default();
        while let Some(current_node_pointer) = node_stack.pop() {
            info_span!("iteration", ?current_node_pointer);
            let current_node = current_node_pointer.as_ref();
            trace!("constructing from node {:?}", current_node);

            ancestors.clear();
            current_node.get_ancestors(&mut ancestors);
            trace!("node has {} ancestors", ancestors.len());
            self.diagnostics.send_node_depth(ancestors.len());
            current_node.get_positions(&mut positions, ancestors.as_slice());

            let current_position = positions.last().expect("get_ancestor_positions output no positions");
            trace!("current node position\n{}", current_position.as_2d_string());

            if ancestors.len() >= max_depth {
                trace!("branch has depth {}; max depth is {max_depth}. Will not create children.", ancestors.len());
                trace!("node_stack.len() = {}", node_stack.len());
                coordinator.node_constructed(current_node_pointer);
                continue;
            }

            let current_node_children = current_node.create_children(
                current_node_pointer.clone(),
                current_position,
                positions.as_slice(),
                &mut current_node_children_storage);

            if current_node.conclusion().is_none() {
                trace!("node is branch; adding children to stack.");
                trace!("current_node_children.len() = {}", current_node_children.len());
                for child in current_node_children {
                    let add_result = self.add_node(shard, child.clone());
                    if !add_result.is_leaf() {
                        trace!("adding {:?} to node stack", add_result.node_pointer());
                        node_stack.push(add_result.node_pointer().clone());
                    } else {
                        coordinator.node_constructed(add_result.node_pointer().clone())
                    }
                }
                trace!("node_stack.len() = {}", node_stack.len());
            }

            coordinator.node_constructed(current_node_pointer);
        }
    }

    fn tally_wins(this: Arc<Self>, thread_subscriber_factory: Arc<dyn ThreadSubscriberFactory>, coordinator: Arc<ConstructionCoordinator>) -> Vec<JoinHandle<()>> {
        let mut threads = vec![];
        for i in 0..coordinator.tallier_count {
            info!("Creating tally thread {i}");
            let this = this.clone();
            let coordinator = coordinator.clone();
            let subscriber = thread_subscriber_factory.create("tally", i).unwrap();
            threads.push(thread::spawn(move || {
                tracing::subscriber::with_default(subscriber, move || {
                    coordinator.starting_tallying();
                    while let Some(node_pointer) = coordinator.get_next_constructed_node() {
                        node_pointer.as_ref().tally_recursive(&node_pointer, &this.diagnostics);
                    }
                    coordinator.finished_tallying();
                })}));
        }
        threads
    }
}

#[derive(Default)]
pub struct Diagnostics {
    node_depth: AtomicUsize,

    node_count: AtomicUsize,
    tallied_count: AtomicUsize,

    finished: AtomicBool,
}

impl Diagnostics {
    fn toplevel(&self) {
        let mut stdout = std::io::stdout();
        println!();
        println!();
        println!();
        let current_position = match cursor::position() {
            Ok(p) => p,
            Err(_) => { return; }
        };
        loop {
            thread::park_timeout(Duration::from_millis(100));
            let finished = self.finished.load(Ordering::Acquire);
            if finished {
                break;
            }
            let node_depth = self.node_depth.load(Ordering::Acquire);
            let node_count = self.node_count.load(Ordering::Acquire);
            let tallied_count = self.tallied_count.load(Ordering::Acquire);

            let _ = execute!(
                stdout,
                cursor::MoveTo(current_position.0, current_position.1 - 3),
                style::Print(format!("Max node depth: {node_depth}")),
                cursor::MoveToNextLine(1),
                style::Print(format!("Nodes constructed: {node_count}")),
                cursor::MoveToNextLine(1),
                style::Print(format!("Nodes tallied: {tallied_count}")),
                cursor::MoveToNextLine(1),
            );
        }
    }

    pub fn send_node_depth(&self, depth: usize) {
        self.node_depth.fetch_max(depth, Ordering::AcqRel);
    }

    pub fn increment_node_count(&self) {
        self.node_count.fetch_add(1, Ordering::AcqRel);
    }

    pub fn clear_node_count(&self) {
        self.node_count.store(0, Ordering::Release);
    }

    pub fn increment_tallied_count(&self) {
        self.tallied_count.fetch_add(1, Ordering::AcqRel);
    }
}

struct DiagnosticsHandle {
    state: Arc<Diagnostics>,
    thread_handle: Option<JoinHandle<()>>,
}

impl DiagnosticsHandle {
    pub fn new() -> Self {
        let state = Arc::new(Diagnostics::default());

        let state_ref = state.clone();
        let thread_handle = thread::Builder::new().name("diagnostics".to_string()).spawn(move || {
            state_ref.toplevel();
        }).ok();


        Self {
            state: state.clone(),
            thread_handle,
        }
    }

    pub fn state_clone(&self) -> Arc<Diagnostics> {
        self.state.clone()
    }

    pub fn finished(mut self) {
        self.state.finished.store(true, Ordering::Release);
        if let Some(handle) = mem::take(&mut self.thread_handle) {
            handle.thread().unpark();
            for _ in 0..2 {
                if handle.is_finished() {
                    break;
                }
                thread::sleep(Duration::from_millis(1));
            }
        }
    }
}
