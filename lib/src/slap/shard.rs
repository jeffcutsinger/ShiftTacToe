use std::ptr::NonNull;

use super::chunk::Chunk;
use super::CHUNK_DEFAULT_SIZE;

pub struct Shard<T> {
    chunks: Vec<Chunk<T>>,
    default_chunk_size: usize,
}

impl<T> Shard<T> {
    pub fn new(default_chunk_size: usize) -> Self {
        let default_chunk_size =
            if default_chunk_size != 0 {
                default_chunk_size
            } else {
                CHUNK_DEFAULT_SIZE
            };

        Self { chunks: vec![], default_chunk_size }
    }

    pub fn push(&mut self, element: T) -> NonNull<T> {
        if self.chunks.is_empty() {
            self.chunks.push(Chunk::<T>::allocate(self.default_chunk_size));
        }

        let mut last_chunk_index = self.chunks.len() - 1;
        if self.chunks[last_chunk_index].is_full() {
            self.chunks.push(Chunk::<T>::allocate(self.default_chunk_size));
            last_chunk_index += 1;
        }

        self.chunks[last_chunk_index].push(element)
    }

    pub fn get(&self, mut index: usize) -> Option<NonNull<T>> {
        for chunk in 0..self.chunks.len() {
            if index < self.chunks[chunk].len() {
                let result = self.chunks[chunk].get(index);
                assert!(result.is_some());
                return result;
            }
            index -= self.chunks[chunk].len();
        }
        None
    }

    pub fn len(&self) -> usize {
        self.chunks.iter().map(|c| c.len()).sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn basic_test() {
        let chunk_size = 2;
        let num_elements = 7;
        let mut shard = Shard::new(chunk_size);
        for i in 0..num_elements {
            let element_ptr = shard.push(i);
            assert_eq!(unsafe { *element_ptr.as_ref() }, i);
        }

        for i in 0..num_elements {
            let ptr = shard.get(i);
            assert!(ptr.is_some());
            let ptr = ptr.unwrap();
            assert_eq!(unsafe { *ptr.as_ref() }, i);
        }

        assert_eq!(shard.get(num_elements), None);

        assert_eq!(shard.len(), num_elements);
        assert_eq!(shard.chunks.len(), (num_elements / chunk_size) + if num_elements % chunk_size != 0 { 1 } else { 0 });
    }

}
