use std::alloc::{self, Layout, handle_alloc_error, LayoutError};
use std::ptr::{self, NonNull};

use tracing::error;

use thiserror::Error;

pub struct Chunk<T> {
    elements: NonNull<T>,
    length: usize,
    capacity: usize,
}

#[derive(Debug, Error)]
pub enum AllocationError {
    #[error("Layout creation failed. Capacity/chunk size {} is likely too large: {}", .0, .1)]
    LayoutCreationFailed(usize, LayoutError),

    #[error("Could not allocate for layout {:?}. Capacity/chunk size ", .0)]
    AllocationFailed(Layout),
}

unsafe impl<T> Send for Chunk<T> {}

impl<T> Chunk<T> {
    pub fn try_allocate(capacity: usize) -> Result<Self, AllocationError> {
        let layout = Layout::array::<T>(capacity).map_err(|err| AllocationError::LayoutCreationFailed(capacity, err))?;
        let ptr = unsafe { alloc::alloc(layout) };
        let elements = match NonNull::new(ptr as *mut T) {
            Some(e) => e,
            None => return Err(AllocationError::AllocationFailed(layout)),
        };

        Ok(Self { elements, length: 0, capacity })
    }

    pub fn allocate(capacity: usize) -> Self {
        match Self::try_allocate(capacity) {
            Ok(chunk) => chunk,
            Err(AllocationError::LayoutCreationFailed(_capacity, layout_error)) => panic!("{}", layout_error),
            Err(AllocationError::AllocationFailed(layout)) => handle_alloc_error(layout),
        }
    }

    pub fn is_full(&self) -> bool {
        self.length == self.capacity
    }

    pub fn push(&mut self, element: T) -> NonNull<T> {
        assert!(self.length < self.capacity, "Length ({}) was not less than capacity ({})", self.length, self.capacity);
        let pointer = NonNull::new(unsafe { self.elements.as_ptr().add(self.length) }).unwrap();
        unsafe { ptr::write(pointer.as_ptr(), element) }
        self.length += 1;
        pointer
    }

    pub fn get(&self, index: usize) -> Option<NonNull<T>> {
        if index < self.length {
            let result = NonNull::new(unsafe { self.elements.as_ptr().add(index) });
            assert!(result.is_some());
            result
        } else {
            None
        }
    }

    pub fn len(&self) -> usize {
        self.length
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::panic;

    #[test]
    fn basic_test() {
        let capacity = 4;
        let mut chunk = Chunk::allocate(capacity);
        for i in 0..capacity {
            chunk.push(i);
        }

        for i in 0..capacity {
            let ptr = chunk.get(i);
            assert!(ptr.is_some());
            let ptr = ptr.unwrap();
            assert_eq!(unsafe { *ptr.as_ref() }, i);
        }

        assert!(chunk.is_full());
        assert!(chunk.get(capacity).is_none());

        let err = panic::catch_unwind(move || chunk.push(capacity));
        assert!(err.is_err());
    }

    #[test]
    fn layout_failure() {
        let capacity = usize::try_from(isize::MAX).unwrap() + 1;
        assert!(matches!(Chunk::<u8>::try_allocate(capacity), Err(AllocationError::LayoutCreationFailed(_, _))));
    }

    #[test]
    fn allocation_failure() {
        let capacity = usize::try_from(isize::MAX).unwrap();
        assert!(matches!(Chunk::<u8>::try_allocate(capacity), Err(AllocationError::AllocationFailed(_))));
    }
}
