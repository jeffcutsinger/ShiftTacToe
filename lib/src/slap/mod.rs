//! Sharded, low allocation, append-only, pinned memory storage.
//!
//! - Sharded: the storage is broken into thread-local pieces.
//! - Low allocation: elements are allocated by chunks of a configurable size to reduce the total number of heap allocations.
//! - Append-only: elements cannot be deleted until the whole structure is deleted.
//! - Pinned: elements will always stay at the same location. In combination with the above, pointers will be valid for as long as the structure is in scope.

pub mod chunk;
pub mod shard;
pub mod slap;

pub const CHUNK_DEFAULT_SIZE: usize = 1 << 20;
pub use shard::Shard;
pub use slap::{LockedSlap, Slap};
