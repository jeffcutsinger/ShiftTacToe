use std::mem;
use std::ptr::NonNull;
use std::sync::{Mutex, MutexGuard};

use super::shard::Shard;
use super::CHUNK_DEFAULT_SIZE;

#[derive(Default)]
pub struct Slap<T> {
    shards: Mutex<Vec<Shard<T>>>,
    default_chunk_size: usize,
}

impl<T> Slap<T> {
    pub fn new(shard_capacity: usize, default_chunk_size: usize) -> Self {
        assert!(mem::size_of::<T>() != 0, "Zero-sized types are unsupported.");
        let default_chunk_size = if default_chunk_size != 0 { default_chunk_size } else { CHUNK_DEFAULT_SIZE };
        let shards = Mutex::new(Vec::with_capacity(shard_capacity));

        Self { shards, default_chunk_size }
    }

    pub fn create_shard_and_take(&self, default_chunk_size: usize) -> Shard<T> {
        Shard::new(if default_chunk_size == 0 { self.default_chunk_size } else { default_chunk_size })
    }

    fn shards<'this>(&'this self) -> MutexGuard<'this, Vec<Shard<T>>> {
        self.shards.lock().expect("Shards lock was poisoned")
    }

    pub fn take_shard(&self) -> Option<Shard<T>> {
        self.shards().pop()
    }

    pub fn release_shard(&self, shard: Shard<T>) {
        self.shards().push(shard)
    }

    pub fn shard_count(&self) -> usize {
        self.shards().len()
    }

    pub fn node_count(&self) -> usize {
        self.shards().iter().map(|s| s.len()).sum()
    }

    pub fn lock<'this>(&'this self) -> LockedSlap<'this, T> {
        LockedSlap(self.shards())
    }
}

pub struct LockedSlap<'slap, T>(MutexGuard<'slap, Vec<Shard<T>>>);

impl<'lock, 'slap, T> IntoIterator for &'lock LockedSlap<'slap, T> {
    type Item = NonNull<T>;

    type IntoIter = ShardsIter<'lock, T>;

    fn into_iter(self) -> Self::IntoIter {
        ShardsIter::new(self.0.as_ref())
    }
}

pub struct ShardsIter<'lock, T> {
    shards: &'lock Vec<Shard<T>>,
    current_shard_index: usize,
    current_element_index: usize,
}

impl<'lock, T> ShardsIter<'lock, T> {
    fn new(shards: &'lock Vec<Shard<T>>) -> Self {
        Self {
            shards,
            current_shard_index: 0,
            current_element_index: 0,
        }
    }
}

impl<'lock, T> Iterator for ShardsIter<'lock, T> {
    type Item = NonNull<T>;

    fn next(&mut self) -> Option<Self::Item> {
        let mut non_empty_shard = None;
        loop {
            if self.current_shard_index >= self.shards.len() {
                break;
            }
            let shard = &self.shards[self.current_shard_index];
            if shard.len() != 0 {
                non_empty_shard = Some(shard);
                break;
            }
            self.current_shard_index += 1;
        }
        let Some(shard) = non_empty_shard else {
            return None;
        };

        assert!(self.current_element_index < shard.len(), "current_element_index: {}, shard.len: {}", self.current_element_index, shard.len());
        let result = shard.get(self.current_element_index).expect("self.current_element_index < shard.len() but .get returned None");

        self.current_element_index += 1;
        if self.current_element_index >= shard.len() {
            self.current_element_index = 0;
            self.current_shard_index += 1;
        }
        return Some(result);
    }
}
