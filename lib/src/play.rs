use crate::constants::PLAY_COUNT_USIZE;

use cfg_if::cfg_if;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum Play {
    ShiftLeft { row: u8 },
    ShiftRight { row: u8 },
    DropPiece { column: u8 },
}

impl Play {
    pub fn all_plays() -> std::slice::Iter<'static, Play> {
        ALL_PLAYS.iter()
    }

    pub fn invert(self) -> Option<Self> {
        match self {
            Self::ShiftLeft { row } => Some(Self::ShiftRight { row }),
            Self::ShiftRight { row } => Some(Self::ShiftLeft { row }),
            Self::DropPiece { .. } => None,
        }
    }
}

cfg_if! {
    if #[cfg(feature="three_by_three")] {
        static ALL_PLAYS: [Play; PLAY_COUNT_USIZE] = [
            Play::ShiftLeft { row: 0 },
            Play::ShiftLeft { row: 1 },
            Play::ShiftLeft { row: 2 },
            Play::ShiftRight { row: 0 },
            Play::ShiftRight { row: 1 },
            Play::ShiftRight { row: 2 },
            Play::DropPiece { column: 0 },
            Play::DropPiece { column: 1 },
            Play::DropPiece { column: 2 },
        ];
    } else if #[cfg(feature="two_by_two")] {
        static ALL_PLAYS: [Play; PLAY_COUNT_USIZE] = [
            Play::ShiftLeft { row: 0 },
            Play::ShiftLeft { row: 1 },
            Play::ShiftRight { row: 0 },
            Play::ShiftRight { row: 1 },
            Play::DropPiece { column: 0 },
            Play::DropPiece { column: 1 },
        ];
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn count_matches() {
        assert_eq!(Play::all_plays().count(), PLAY_COUNT_USIZE);
    }
}
